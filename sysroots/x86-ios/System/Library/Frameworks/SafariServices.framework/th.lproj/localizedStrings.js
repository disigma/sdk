var localizedStrings = new Object;

localizedStrings["Loading Next Page..."] = "กำลังโหลดหน้าถัดไป…";
localizedStrings["Page %@"] = "หน้า %@";
localizedStrings["Page %@ of %@"] = "หน้า %@ จาก %@";
localizedStrings["Connect to the Internet to view remaining pages."] = "เชื่อมต่อกับอินเทอร์เน็ตเพื่อดูหน้าที่เหลือ";
localizedStrings["Decrease text size"] = "ลดขนาดข้อความ";
localizedStrings["Increase text size"] = "เพิ่มขนาดข้อความ";
